﻿using ProjeYemek.Areas.Admin.Models;
using ProjeYemek.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjeYemek.Areas.Admin.Controllers
{
    public class TuzlularController : AdminController
    {
        projeyemekEntities db = new projeyemekEntities();
        // GET: Admin/Tuzlular
        public ActionResult Index()
        {
            var model = db.Tuzlular.OrderByDescending(x => x.ID).ToList();
            return View(model);
        }
        public ActionResult Add()
        {
            return View();
        }
        const string imageFolderPath = "/Content/images/";
        public ActionResult AddTuzlu(TuzlularModel model)
        {
            if (ModelState.IsValid)
            {

                string fileName = string.Empty;
                //Dosya kaydetme
                if (model.Foto != null && model.Foto.ContentLength > 0)
                {
                    fileName = model.Foto.FileName;
                    var path = Path.Combine(Server.MapPath("~" + imageFolderPath), fileName);
                    model.Foto.SaveAs(path);
                }


                //EF nesnesi oluşturma
                Tuzlular tuzlu = new Tuzlular();
                tuzlu.KisiAdedi = model.KisiAdedi;
                tuzlu.PisirmeSuresi = model.PisirmeSuresi;
                tuzlu.Zaman = model.Zaman;
                tuzlu.Baslik = model.Baslik;
                tuzlu.Tarif = model.Tarif;
                tuzlu.ResimYolu = imageFolderPath + fileName;

                db.Tuzlular.Add(tuzlu);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model = db.Tatlilar.Find(id);
            //db.Tatlilar.FirstOrDefault(x => x.ID == id);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(Tatlilar model)
        {
            if (ModelState.IsValid)
            {
                db.Tatlilar.Attach(model);
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                return View(model);
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var model = db.Tuzlular.Find(id);
            return View(model);
        }

        [HttpGet]
        public ActionResult DeleteConfirm(int id)
        {
            var model = db.Tuzlular.Find(id);
            db.Tuzlular.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


    }
}