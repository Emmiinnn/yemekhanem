﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjeYemek.Areas.Admin.Models
{
    public class RolKullaniciEkleModel
    {
        public string KullaniciAdi { get; set; }
        public string RolAdi { get; set; }

    }
}