﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjeYemek.Areas.Admin.Models
{
    public class SliderModel
    {
        public int ID { get; set; }
        public string SliderText { get; set; }
        public Nullable<System.DateTime> BaslangicTarihi { get; set; }
        public Nullable<System.DateTime> BitisTarihi { get; set; }
        public HttpPostedFileBase SliderFoto { get; set; }
        //ResimYolu ---> Ben ekledim.
        public string ResimYolu { get; set; }

    }
}