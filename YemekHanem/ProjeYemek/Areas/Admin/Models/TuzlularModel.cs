﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjeYemek.Areas.Admin.Models
{
    public class TuzlularModel
    {
        public int ID { get; set; }
        public string Baslik { get; set; }
        public string ResimYolu { get; set; }
        public Nullable<System.DateTime> Zaman { get; set; }
        public string PisirmeSuresi { get; set; }
        public string KisiAdedi { get; set; }
        public string Tarif { get; set; }
        public HttpPostedFileBase Foto { get; set; }
    }
}