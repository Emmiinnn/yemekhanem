﻿using ProjeYemek.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace ProjeYemek.Controllers
{
    public class HomeController : Controller
    {
        projeyemekEntities db = new projeyemekEntities();
        public ActionResult Index()
        {
            using (projeyemekEntities db = new projeyemekEntities())
            {
                VerilerDTO data = new VerilerDTO();
                data.sliders= db.Slider.Take(3).ToList();
                data.tatli1 = db.Tatlilar.OrderByDescending(x => x.Zaman).Take(1).ToList();
                data.tatli2 = db.Tatlilar.OrderByDescending(x=>x.Zaman).Take(3).ToList();
                data.tuzlu=db.Tuzlular.OrderByDescending(x => x.Zaman).Take(3).ToList();
                data.salata = db.Salatalar.OrderByDescending(x => x.Zaman).Take(3).ToList();
                return View(data);
            }
        }
        //[ChildActionOnly]//Sadece anasayfada render edilip calistirlir
        public ActionResult Tuzlular()
        {
            var liste = db.Tuzlular.ToList();
            return View(liste);
        }
        public ActionResult Salatalar()
        {
            var liste = db.Salatalar.ToList();
            return View(liste);
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Tatlilar()
        {
            var liste = db.Tatlilar.ToList();
            return View(liste);

        }
        public ActionResult Recipe()
        {
            return View();
        }
        public ActionResult GetirTatli(int id)
        {
            var model = db.Tatlilar.Find(id);
            return View(model);
        }
        public ActionResult GetirTuzlu(int id)
        {
            var model = db.Tuzlular.Find(id);
            return View(model);
        }
        public ActionResult GetirSalata(int id)
        {
            var model = db.Salatalar.Find(id);
            return View(model);
        }

    }
    public class VerilerDTO
    {
        public List<Slider> sliders { get; set; }
        public List<Tatlilar> tatli1 { get; set; }
        public List<Tatlilar> tatli2 { get; set; }
        public List<Tuzlular> tuzlu { get; set; }
        public List<Salatalar> salata { get; set; }
    }
}