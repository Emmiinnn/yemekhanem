//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjeYemek.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tatlilar
    {
        public int ID { get; set; }
        public string Baslik { get; set; }
        public string ResimYolu { get; set; }
        public Nullable<System.DateTime> Zaman { get; set; }
        public string PisirmeSuresi { get; set; }
        public string KisiAdedi { get; set; }
        public string Tarif { get; set; }
        public byte[] Foto { get; set; }
    }
}
